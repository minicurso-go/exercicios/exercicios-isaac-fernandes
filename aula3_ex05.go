package main

import "fmt"

func main() {
	var num int
	fmt.Print("digite um número: ")
	fmt.Scan(&num)

	for i := 1; i <= num; i++ {
		if num%i == 0 {
			println(i)
		}
	}
}
