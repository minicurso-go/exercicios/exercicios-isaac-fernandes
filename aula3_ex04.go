package main

import "fmt"

func main() {
	for n := 1; n < 100+1; n++ {
		if n%2 != 0 && n%5 == 0 {
			fmt.Println("FizzBuzz")
		} else if n%5 == 0 {
			fmt.Println("Buzz")
		} else if n%2 != 0 {
			fmt.Println("Fizz")
		} else {
			fmt.Println(n)
		}
	}
}
