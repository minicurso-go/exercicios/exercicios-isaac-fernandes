package main

import "fmt"

func main() {
	for n := 1; n < 30+1; n++ {
		if n%3 == 0 {
			fmt.Println(n)
		}
	}
}
