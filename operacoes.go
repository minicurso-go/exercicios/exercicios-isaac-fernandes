package main

import "fmt"

func main() {
	var num1, num2 float64
	var operacao int

	fmt.Println("digite o primeiro numero:")
	fmt.Scanln(&num1)

	fmt.Println("digite o segundo numero:")
	fmt.Scanln(&num2)

	fmt.Println("qual operacao quer fazer:")
	fmt.Println("1 - soma")
	fmt.Println("2 - subtração")
	fmt.Println("3 - divisão")
	fmt.Println("4 - multiplicação")
	fmt.Scanln(&operacao)

	switch operacao {
	case 1:
		resultado := num1 + num2
		fmt.Println("resultado:", resultado)
	case 2:
		resultado := num1 - num2
		fmt.Println("resultado:", resultado)
	case 3:
		if num2 != 0 {
			resultado := num1 / num2
			fmt.Println("resultado:", resultado)
		} else {
			fmt.Println("nao pode dividir por zero.")
		}
	case 4:
		resultado := num1 * num2
		fmt.Println("resultado:", resultado)
	default:
		fmt.Println("código errado.")
	}
}