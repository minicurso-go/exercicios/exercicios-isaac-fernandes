package main

import "fmt"

func main() {
    var x, y float64

    fmt.Println("digite as coordenadas x e y (separadas por espaço):")
    fmt.Scanln(&x, &y)

    if x == 0 && y == 0 {
        fmt.Println("ponto de origem")
    } else if x == 0 {
        fmt.Println("está no eixo Y")
    } else if y == 0 {
        fmt.Println("está no eixo X")
    } else if x > 0 && y > 0 {
        fmt.Println("quadrante 1")
    } else if x < 0 && y > 0 {
        fmt.Println("quadrante 2")
    } else if x < 0 && y < 0 {
        fmt.Println("quadrante 3")
    } else {
        fmt.Println("quadrante 4")
    }
}