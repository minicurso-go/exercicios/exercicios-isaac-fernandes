package main

import "fmt"

func main() {
	var num int
	fmt.Print("digite um número:")
	fmt.Scan(&num)

	for i := 1; i < 10+1; i++ {
		fmt.Println(num, "x", i, "=", i*num)
	}
}
