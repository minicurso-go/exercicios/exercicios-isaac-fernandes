package main

import "fmt"

func main() {
	var a, b, c float64
	fmt.Print("valor a: ")
	fmt.Scanln(&a)
	fmt.Println("valor b: ")
	fmt.Scanln(&b)
	fmt.Print("valor c: ")
	fmt.Scanln(&c)

	triangulo := (a * c) / 2
	circulo := 3.14159 * c * c
	trapezio := ((a + b) * c) / 2
	quadrado := b * b
	retangulo := a * b

	fmt.Printf("TRIANGULO: %.3f\n", triangulo)
	fmt.Printf("CIRCULO: %.3f\n", circulo)
	fmt.Printf("TRAPEZIO: %.3f\n", trapezio)
	fmt.Printf("QUADRADO: %.3f\n", quadrado)
	fmt.Printf("RETANGULO: %.3f\n", retangulo)
}
